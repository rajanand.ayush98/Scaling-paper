
# Scaling
Scaling a business means setting the stage to enable and support growth in your company. It means having the ability to grow without being hampered. It requires planning, some funding and the right systems, staff, processes, technology and partners.


## Load balancing
Google Cloud offers server-side load balancing so you can distribute incoming traffic across multiple virtual machine (VM) instances. Load balancing provides the following benefits:

* Scale your app
* Support heavy traffic
* Detect and automatically remove unhealthy VM instances using health checks. Instances that become healthy again are automatically re-added.
* Route traffic to the closest virtual machine

Google Cloud load balancing is a managed service, which means its components are redundant and highly available. If a load balancing component fails, it is restarted or replaced automatically and immediately.

Google Cloud offers several different types of load balancing that differ in capabilities, usage scenarios, and how you configure them. See Google Cloud load balancing documentation for descriptions.

## Autoscaling
Compute Engine offers autoscaling to automatically add or remove VM instances from a managed instance group based on increases or decreases in load. Autoscaling lets your apps gracefully handle increases in traffic, and it reduces cost when the need for resources is lower. After you define the autoscaling policy, the auto-scaler performs automatic scaling based on the measured load.


## Vertical Vs Horizontal Scaling
The heart of the difference is the approach to adding computing resources to your infrastructure. With vertical scaling (a.k.a. “scaling up”), you’re adding more power to your existing machine. In horizontal scaling (a.k.a. “scaling out”), you get the additional resources into your system by adding more machines to your network, sharing the processing and memory workload across multiple devices.

## Why Scaling Out Is Better Than Up ?
When you’re choosing between horizontal scaling and vertical scaling, you also have to consider what’s at stake when you scale up versus scale out.

Horizontal scaling is almost always more desirable than vertical scaling because you don’t get caught in a resource deficit. Instead of taking your server offline while you’re scaling up to a better one, horizontal scaling lets you keep your existing pool of computing resources online while adding more to what you already have. When your app is scaled horizontally, you have the benefit of elasticity.

You can do exactly this when your infrastructure is hosted in a Managed Cloud environment.

Other benefits of scaling out in a cloud environment include:

* Instant and continuous availability
* No limit to hardware capacity
* Cost can be tied to use
* You’re not stuck always paying for peak demand
* Built-in redundancy
* Easy to size and resize properly to your needs


## How To Achieve Effective Horizontal Scaling ?
There are important best practices to keep in mind to make your service offering super compatible with horizontal scaling.

The first is to make your application stateless on the server side as much as possible. Any time your application has to rely on server-side tracking of what it’s doing at a given moment, that user session is tied inextricably to that particular server. If, on the other hand, all session-related specifics are stored browser-side, that session can be passed seamlessly across literally hundreds of servers. The ability to hand a single session (or thousands or millions of single sessions) across servers interchangeably is the very epitome of horizontal scaling.

The second goal to keep square in your sights is to develop your app with a service-oriented architecture. The more your app is comprised of self-contained but interacting logical blocks, the more you’ll be able to scale each of those blocks independently as your use load demands. Be sure to develop your app with independent web, application, caching and database tiers. This is critical for realizing cost savings – because without this micro-service architecture, you’re going to have to scale up each component of your app to the demand levels of services tier getting hit the hardest.

### Cloud-Friendly, Horizontal-Scaling App And DevOps
You’ve worked hard developing your app. Now it’s time to get it on the market and ready to handle staggering growth. A Managed AWS Cloud and DevOps Automation solution is the most effective way to ensure your app scales to success.


## References
- [Load balancing](https://cloud.google.com/compute/docs/load-balancing-and-autoscaling)
- [Vertical vs Horizontal](https://www.missioncloud.com/blog/horizontal-vs-vertical-scaling-which-is-right-for-your-app#:~:text=With%20vertical%20scaling%20a.k.a.%20%E2%80%9Cscaling,memory%20workload%20across%20multiple%20devices.)
- [Tips for writing technical paper](https://cs.stanford.edu/people/widom/paper-writing.html)